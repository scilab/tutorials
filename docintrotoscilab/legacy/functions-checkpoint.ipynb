{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "288e187d-7e02-4769-94f6-0132e141c2ed",
   "metadata": {},
   "source": [
    "# Functions"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10fd7abf-5ddb-4cd9-b96e-96967256b18b",
   "metadata": {},
   "source": [
    "## Table of contents\n",
    "1. [Overview](#functions_overview)\n",
    "2. [Defining a function](#functions_defining)\n",
    "3. [Function libraries](#functions_lib)\n",
    "4. [Managing output arguments](#functions_output)\n",
    "5. [Levels in the call stack](#functions_callstack)\n",
    "6. [The **return** statement](#functions_return)\n",
    "6. [Debugging functions with **pause**](#functions_pause)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c6c842e5-95c7-4821-b6f1-c14342c43568",
   "metadata": {},
   "source": [
    "In this section, we present Scilab functions. We analyze the way to define a new function and the method\r\n",
    "to load it into Scilab. We present how to create and load a **library**, which is a collection of functions. We also present how to manage input and output arguments. Finally, we present how to debug a function using the **pause** statement."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3abf3caf-cd30-4460-b74f-3cdb2565f82b",
   "metadata": {},
   "source": [
    "## Overview <a name=\"functions_overview\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b389bcc5-857e-47bb-a042-3e52f425ea77",
   "metadata": {},
   "source": [
    "Gathering various steps into a reusable function is one of the most common tasks of a Scilab developer. \r\n",
    "The most simple calling sequence of a function is the following: `outvar = myfunction ( invar )` \r\n",
    "where the following list presents the various variables used in the syntax:\r\n",
    "- **myfunction** is the name of the function,\r\n",
    "- **invar** is the name of the input arguments,\r\n",
    "- **outvar** is the name of the output arguments.\r\n",
    "\r\n",
    "The values of the input arguments are not modified by the function, while the \r\n",
    "values of the output arguments are actually modified by the function.\r\n",
    "\r\n",
    "We have in fact already met several functions in this document.\r\n",
    "The **sin** function, in the **y=sin(x)** statement, \r\n",
    "takes the input argument **x** and returns the result in the output argument **y**.\r\n",
    "In Scilab vocabulary, the input arguments are called the **right hand side** and the output arguments are called the **left hand side**.\r\n",
    "\r\n",
    "Functions can have an arbitrary number of input and output arguments so that the complete syntax for a function which has a fixed number of arguments is the following: `[o1, ..., on] = myfunction ( i1, ..., in )`\r\n",
    "The input and output arguments are separated by commas **,**.\r\n",
    "Notice that the input arguments are surrounded by opening and closing parentheses,\r\n",
    "while the output arguments are surrounded by opening and closing **square** brackets.\r\n",
    "\r\n",
    "In the following Scilab session, we show how to compute the $LU$ decomposition of the Hilbert matrix. The following session shows how to create a matrix with the **testmatrix** function, which takes two input arguments, and returns one matrix. Then, we use the **lu** function, which takes one input argument and returns two or three arguments depending on the provided output variables. If the third argument **P** is provided, the permutationmatrix is returned.\r\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "ce111290-32da-469f-8176-726fe7d641fc",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      " A  = \n",
      "\n",
      "   4.  -6. \n",
      "  -6.   12.\n",
      "\n",
      " L  = \n",
      "\n",
      "  -0.6666667   1.\n",
      "   1.          0.\n",
      " U  = \n",
      "\n",
      "  -6.   12.\n",
      "   0.   2. \n",
      "\n",
      " L  = \n",
      "\n",
      "   1.          0.\n",
      "  -0.6666667   1.\n",
      " U  = \n",
      "\n",
      "  -6.   12.\n",
      "   0.   2. \n",
      " P  = \n",
      "\n",
      "   0.   1.\n",
      "   1.   0.\n",
      "\n"
     ]
    }
   ],
   "source": [
    "A = testmatrix(\"hilb\",2)\n",
    "[L, U] = lu(A)\n",
    "[L, U, P] = lu(A)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8eb91adf-87f9-4915-b93c-8fb214c4b94a",
   "metadata": {},
   "source": [
    "Notice that the behavior of the **lu** function actually changes when three output arguments are provided: the two rows of the matrix **L** have been swapped. More specifically, when two output arguments are provided, the decomposition $A=LU$ is provided (the statement **A-L*U** checks this). When three output arguments are provided, permutations are performed so that the decomposition $PA=LU$ is provided (the statement **P*A-L*U** can be used to check this). In fact, when two output arguments are provided, the permutations are applied on the **L** matrix. This means that the **lu** function knows how many input and output arguments are provided to it, and changes its algorithm accordingly. We will not present in this document how to provide this feature, i.e. a variable number of input or output arguments. But we must keep in mind that this is possible in the Scilab language.\r\n",
    "\r\n",
    "The commands provided by Scilab to manage functions are \r\n",
    "| Name | Description |\r\n",
    "| ----- | ---------- |\r\n",
    "| function | opens a function definition |\r\n",
    "| endfunction | closes a function definition |\r\n",
    "| argn | number of input/output arguments in a function call |\r\n",
    "| varargin | variable numbers of arguments in an input argument list |\r\n",
    "| varargout | variable numbers of arguments in an output argument list |\r\n",
    "| get_function_path | get source file path of a library function |\r\n",
    "| getd | getting all functions defined in a directory |\r\n",
    "| head_comments | display Scilab function header comments |\r\n",
    "| macrovar | variables of function |\r\n",
    "\r\n",
    "In the next sections, we will present some of the most commonly used commands."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "856ede3d-871e-4626-993a-97a1c4c6be3d",
   "metadata": {},
   "source": [
    "## Defining a function <a name=\"functions_defining\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f5e083f0-5205-41b9-9714-731d0a4c8677",
   "metadata": {},
   "source": [
    "To define a new function, we use the **function** and **endfunction** Scilab keywords. In the following example, we define the function **myfunction**, which takes the input argument **x**, multiplies it by 2, and returns the value in the output argument **y**.\n",
    "\n",
    "```\n",
    "function y = myfunction ( x )\n",
    "  y = 2 * x\n",
    "endfunction\n",
    "```\n",
    "\n",
    "The statement **function y = myfunction ( x )** is the **header** of the function while the **body** of the function is made of the statement **y = 2 * x**. The body of a function may contain one, two or more statements.\n",
    "\n",
    "There are at least three possibilities to define the previous function in Scilab.\n",
    "- The first solution is to type the script directly into the console in an interactive mode. Notice that, once the \"**function y = myfunction ( x )**\" statement has been written and the enter key is typed in, Scilab creates a new line in the console, waiting for the body of the function.\n",
    "When the \"**endfunction**\" statement is typed in the console, Scilab returns back to its normal edition mode. \n",
    "\n",
    "- Another solution is available when the source code of the function is provided in a file. This is the most common case, since functions are generally quite long and complicated. We can simply copy and paste \n",
    "the function definition into the console. When the function definition is short (typically, a dozen lines of source code), this way is very convenient. With the editor, this is very easy, thanks to the **Load into Scilab** feature.\n",
    "\n",
    "- We can also use the **exec** function. Let us consider a Windows system where the previous function is written in the file \"examples-functions.sce\", in the \"C:\\\\myscripts\" directory. The following session gives an example of the use of **exec** to load the previous function.\n",
    "```\n",
    "-->exec(\"C:\\myscripts\\examples-functions.sce\")\n",
    "-->function y = myfunction ( x )\n",
    "-->  y = 2 * x\n",
    "-->endfunction\n",
    "```\n",
    "\n",
    "The **exec** function executes the content of the file as if it were written interactively in the console and displays the various Scilab statements, line after line. The file may contain a lot of source code so that the output may be very long and useless. In these situations, we add the semicolon character **;** at\n",
    "the end of the line.\n",
    "This is what is performed by the **Execute file into Scilab** feature of the editor.\n",
    "```\n",
    "-->exec(\"C:\\myscripts\\examples-functions.sce\" );\n",
    "```\n",
    "\n",
    "Once a function is defined, it can be used as if it was any other Scilab function.\n",
    "```\n",
    "-->exec(\"C:\\myscripts\\examples-functions.sce\");\n",
    "-->y = myfunction ( 3 )\n",
    " y  =\n",
    "    6.  \n",
    "```\n",
    "\n",
    "Notice that the previous function sets the value of the output argument **y**, with the statement **y=2*x**. This is mandatory. In order to see it, we define in the following script a function which sets the variable **z**, but not the output argument **y**.\n",
    "```\n",
    "function y = myfunction ( x )\n",
    "  z = 2 * x\n",
    "endfunction\n",
    "```\n",
    "\n",
    "In the following session, we try to use our function with the input argument **x=1**.\n",
    "```\n",
    "-->myfunction ( 1 )\n",
    " !--error 4 \n",
    "Undefined variable: y\n",
    "at line       4 of function myfunction called by :  \n",
    "myfunction ( 1 )\n",
    "```\n",
    "\n",
    "Indeed, the interpreter tells us that the output variable **y** has not been defined. \n",
    "\n",
    "When we make a computation, we often need more than one function in order to perform all the steps of the algorithm. For example, consider the situation where we need to optimize a system. In this case, we might use an algorithm provided by Scilab, say **optim** for example. First, we define the cost function which \n",
    "is to be optimized, according to the format expected by **optim**. Second, we define a driver, which calls the **optim** function with the required arguments. At least two functions are used in this simple scheme. In practice, a complete computation often requires a dozen of functions, or more. In this case, we may collect our functions in a library and this is the topic of the next section."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cc950639-2ac0-44d1-89d0-c57dbac58085",
   "metadata": {},
   "source": [
    "## Function libraries <a name=\"functions_lib\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28e20ddd-b1ca-4283-9262-a881eb82e779",
   "metadata": {},
   "source": [
    "A function library is a collection of functions defined in the Scilab language and stored in a set of files.\r\n",
    "\r\n",
    "When a set of functions is simple and does not contain any help or any source code in a compiled language \r\n",
    "like C/C++ or Fortran, a library is a very efficient way to proceed. Instead, when we design a Scilab component with unit tests, help pages and demonstration scripts, we develop a **module**. Developing \r\n",
    "a module is both easy and efficient, but requires a more advanced knowledge of Scilab. Moreover, modules are based on function libraries, so that understanding the former makes us master the latter. Modules will not be described in this document. Still, in many practical situations, function libraries allow \r\n",
    "efficient management of simple collections of functions and this is why we describe this system here.\r\n",
    "\r\n",
    "In this section, we describe a very simple library and show how to load it automatically at Scilab startup.\r\n",
    "\r\n",
    "Let us make a short outline of the process of creating and using a library. We assume that we are given a set of **.sci** files containing functions:\r\n",
    "- We create a binary version of the scripts containing the functions. The **genlib** function generates binary versions of the scripts, as well as additional indexing files. \r\n",
    "- We load the library into Scilab. The **lib** function loads a library stored in a particular directory. \r\n",
    "\r\n",
    "Before analyzing an example, let us consider some general rules which must be followed when we design a function library. These rules will then be reviewed in the next example.\r\n",
    "\r\n",
    "The file names containing function definitions should end with the **.sci** extension. This is not mandatory, but helps in identifying the Scilab scripts on a hard drive. \r\n",
    "\r\n",
    "Several functions may be stored in each **.sci** file, but only the first one will be available from outside the file. Indeed, the first function of the file is considered to be the only public function, while the other functions are (implicitly) private functions. \r\n",
    "\r\n",
    "The name of the **.sci** file must be the same as the name of the first function in the file. For example, if the function is to be named **myfun**, then the file containing this function must be **myfun.sci**. This is mandatory in order to make the **genlib** function work properly.\r\n",
    "\r\n",
    "The functions which manage libraries are:\r\n",
    "- **genlib**, build library from functions in a given directory\r\n",
    "- **lib**, library definition\r\n",
    "\r\n",
    "We shall now give a small example of a particular library and give some details about how to actually proceed.\r\n",
    "\r\n",
    "Assume that we use a Windows system and that the **samplelib** directory contains two files:\r\n",
    "- C:\\samplelib\\function1.sci:\r\n",
    "```\r\n",
    "function y = function1 ( x )\r\n",
    "  y = 1 * function1_support ( x )\r\n",
    "endfunction\r\n",
    "function y = function1_support ( x )\r\n",
    "  y = 3 * x\r\n",
    "endfunction\r\n",
    "```\r\n",
    "- C:\\samplelib\\function2.sci:\r\n",
    "```\r\n",
    "function y = function2 ( x )\r\n",
    "  y = 2 * x\r\n",
    "endfunction\r\n",
    "```\r\n",
    "\r\n",
    "In the following session, we generate the binary files with the **genlib** function, which takes as its first argument a string associated with the library name, and takes as its second argument the name of the directory containing the files. Notice that only the functions **function1** and **function2** are publicly available: the **function1_support** function can be used inside the library, but cannot be used outside.\r\n",
    "```\r\n",
    "-->genlib(\"mylibrary\",\"C:\\samplelib\")\r\n",
    "-->mylibrary\r\n",
    " mylibrary  =\r\n",
    "Functions files location : C:\\samplelib\\.\r\n",
    " function1           function2           \r\n",
    "```\r\n",
    "\r\n",
    "The **genlib** function generates the following files in the directory **\"C:\\samplelib\"**:\r\n",
    "- **function1.bin**: the binary version of the **function1.sci** script,\r\n",
    "- **function2.bin**: the binary version of the **function2.sci** script,\r\n",
    "- **lib**: a binary version of the library,\r\n",
    "- **names**: a text file containing the list of functions in the library.\r\n",
    "\r\n",
    "The binary files ***.bin** and the **lib** file are cross-platform in the sense that they work equally well under Windows, Linux or Mac.\r\n",
    "\r\n",
    "Once the **genlib** function has been executed, the two functions are immediately available, as detailed in the following example.\r\n",
    "```\r\n",
    "-->function1(3)\r\n",
    " ans  =\r\n",
    "    9.  \r\n",
    "-->function2(3)\r\n",
    " ans  =\r\n",
    "    6. \r\n",
    "``` \r\n",
    "\r\n",
    "In practical situations, though, we would not generate the library every time it is needed. Once the library is ready, we would like to load the library directly. This is done with the **lib** function, which takes as its first argument the name of the directory containing the library and returns the library, as in the following session.\r\n",
    "\r\n",
    "```\r\n",
    "-->mylibrary = lib(\"C:\\samplelib\\\")\r\n",
    " ans  =\r\n",
    "Functions files location : C:\\samplelib\\.\r\n",
    " function1           function2   \r\n",
    "```\r\n",
    "       \r\n",
    "\r\n",
    "If there are many libraries, it might be inconvenient to manually load all libraries at startup. In practice, the **lib** statement can be written once for all, in Scilab startup file, so that the library is immediately available at startup. The startup directory associated with a particular Scilab installation is stored in the variable ***SCIHOME**, as presented in the following session, for example on Windows."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "35c69fd0-149a-4c01-9b39-6b4c22a874f8",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      " SCIHOME  = \n",
      "\n",
      "  \"C:\\Users\\acs18\\AppData\\Roaming\\Scilab\\scilab-2024.0.0\"\n",
      "\n"
     ]
    }
   ],
   "source": [
    "SCIHOME"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9efbdc59-04d8-4051-80a2-90a9dd0ff75a",
   "metadata": {},
   "source": [
    "In the directory associated with the **SCIHOME** variable, the startup file is **.scilab**. The startup file is automatically read by Scilab at startup. It must be a regular Scilab script (it can contain valid comments). To make our library available at startup, we simply write the following lines in our **.scilab** file.\n",
    "```\n",
    "// Load my favorite library.\n",
    "mylibrary = lib(\"C:\\samplelib\")\n",
    "```\n",
    "\n",
    "With this startup file, the functions defined in the library are available directly at Scilab startup."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d0014cdf-5d1d-459a-9284-086df5cae640",
   "metadata": {},
   "source": [
    "## Managing output arguments <a name=\"functions_output\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d1daa5b2-c8e4-455e-8123-ef1c4c77ea75",
   "metadata": {},
   "source": [
    "In this section, we present the various ways to manage output arguments. A function may have zero or more input and/or output arguments. In the most simple case, the number of input and output arguments is pre-defined and using such a function is easy. But, as we are going to see, even such a simple function can be called in various ways.\n",
    "\n",
    "Assume that the function **simplef** is defined with 2 input arguments and 2 output arguments, as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "65115a0e-64a3-449d-8ead-cff8a8cdc5a9",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    }
   ],
   "source": [
    "function [y1 , y2] = simplef ( x1, x2 )\n",
    "  y1 = 2 * x1\n",
    "  y2 = 3 * x2\n",
    "endfunction"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "75d04c39-5bb0-4b63-b303-efc5ab3a45be",
   "metadata": {},
   "source": [
    "In fact, the number of output arguments of such a function can be 0, 1 or 2. When there is no output argument, the value of the first output argument in stored in the **ans** variable. We may also set the variable **y1** only. Finally, we may use all the output arguments, as expected. The following session presents all these calling sequences."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "d810317f-48a1-49b5-a73e-55e4c2278b76",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      " ans  =\n",
      "\n",
      "   2.\n",
      "\n",
      " y1  = \n",
      "\n",
      "   2.\n",
      "\n",
      " y1  = \n",
      "\n",
      "   2.\n",
      " y2  = \n",
      "\n",
      "   6.\n",
      "\n"
     ]
    }
   ],
   "source": [
    "simplef ( 1 , 2 )\n",
    "y1 = simplef ( 1 , 2 )\n",
    "[y1,y2] = simplef ( 1 , 2 )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "118fedc2-a67f-414f-bde6-482c833e7b24",
   "metadata": {},
   "source": [
    "We have seen that the most basic way of defining functions already allows to manage a variable number of output arguments. There is an even more flexible way of managing a variable number of input and output arguments, based on the **argn**, **varargin** and **varargout** variables. This more advanced topic will not be detailed in this document."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aa05696a-549b-4190-bc3f-2be203c4bf6b",
   "metadata": {},
   "source": [
    "## Levels in the call stack <a name=\"functions_callstack\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ed0acd3d-96f8-4ab7-b946-fe29dcaf5398",
   "metadata": {},
   "source": [
    "Obviously, function calls can be nested, i.e. a function **f** can call a function **g**, which in turn calls a function **h** and so forth. When Scilab starts, the variables which are defined are at the **global** scope. When we are in a function which is called from the global scope, we are one level down \r\n",
    "in the call stack. When nested function calls occur, the current level in the call stack is equal to the number of previously nested calls. The functions inquire about the state of the call stack:\r\n",
    "- **whereami** displays current instruction calling tree\r\n",
    "- **where** gets current instruction calling tree\r\n",
    "\r\n",
    "In the following session, we define 3 functions which are calling one another and we use the function **whereami** to display the current instruction calling tree."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "2b36f4da-ebfc-46ed-8d88-2bfa1096d1e9",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n",
      "\n",
      "\n"
     ]
    }
   ],
   "source": [
    "function y = fmain ( x )\n",
    "  y = 2 * flevel1 ( x )\n",
    "endfunction\n",
    "function y = flevel1 ( x )\n",
    "  y = 2 * flevel2 ( x )\n",
    "endfunction\n",
    "function y = flevel2 ( x )\n",
    "  y = 2 * x\n",
    "  whereami()\n",
    "endfunction"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6f340921-a2e4-418d-a1c4-388445622a14",
   "metadata": {},
   "source": [
    "When we call the function **fmain**, the following output is produced. As we can see, the 3 levels in the call stack are displayed, associated with the corresponding function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "5eb04220-9978-41c7-91fc-f7e860a29d96",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "whereami called at line 3 of macro flevel2\n",
      "flevel2  called at line 2 of macro flevel1\n",
      "flevel1  called at line 2 of macro fmain\n",
      " ans  =\n",
      "\n",
      "   8.\n",
      "\n"
     ]
    }
   ],
   "source": [
    "fmain(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b00e0513-85ce-4e76-a45a-ae4e78c108a7",
   "metadata": {},
   "source": [
    "In the previous example, the various calling levels are the following:\r\n",
    "- level 0 : the global level,\r\n",
    "- level -1 : the body of the **fmain** function,\r\n",
    "- level -2 : the body of the **flevel1** function,\r\n",
    "- level -3 : the body of the **flevel2** function.\r\n",
    "\r\n",
    "These calling levels are displayed in the prompt of the console when we interactively debug a function with the **pause** statement or with breakpoints."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9e5c3233-64ae-4c45-8792-9ca598cb131c",
   "metadata": {},
   "source": [
    "## The **return** statement <a name=\"functions_return\" />"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4ca1ee8e-bf29-451e-9642-e14e55f85f78",
   "metadata": {},
   "source": [
    "Inside the body of a function, the **return** statement immediately stops the function, i.e. it immediately quits the current function. This statement can be used in cases where the remaining of the algorithm is not necessary.\r\n",
    "\r\n",
    "The following function computes the sum of integers from **istart** to **iend**. In regular situations,\r\n",
    "it uses the **sum** function to perform its job. But if the **istart** variable is negative or if the \r\n",
    "**istart<=iend** condition is not satisfied, the output variable **y** is set to 0 and the function immediately returns."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "c940f74a-15a5-45c5-9364-bec9f92988c2",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n"
     ]
    }
   ],
   "source": [
    "function y = mysum ( istart , iend )\n",
    "  if ( istart < 0 ) then\n",
    "    y = 0\n",
    "    return\n",
    "  end\n",
    "  if ( iend < istart ) then\n",
    "    y = 0\n",
    "    return\n",
    "  end\n",
    "  y = sum ( istart : iend )\n",
    "endfunction"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b937f168-83d1-43ba-b970-a719c35b90d2",
   "metadata": {},
   "source": [
    "The following session checks that the **return** statement is correctly used by the **mysum** function.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "9773ca86-f21f-44f6-9d55-0f25a64797e8",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      " ans  =\n",
      "\n",
      "   15.\n",
      "\n",
      " ans  =\n",
      "\n",
      "   0.\n",
      "\n",
      " ans  =\n",
      "\n",
      "   0.\n",
      "\n"
     ]
    }
   ],
   "source": [
    "mysum ( 1 , 5 )\n",
    "mysum(-1, 5)\n",
    "mysum(2, 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d873eb96-c64e-49a2-a137-590b757ba370",
   "metadata": {},
   "source": [
    "Some developers state that using several **return** statements in a function is generally a bad practice. \r\n",
    "Indeed, we must take into account the increased difficulty of debugging such a function, because the algorithm may suddenly quit the body of the function. The user may get confused about what exactly caused the function to return.\r\n",
    "\r\n",
    "This is why, in practice, the **return** statement should be used with care, and certainly not in every function. The rule to follow is that the function should return only at its very last line.\r\n",
    "Still, in particular situations, using **return** can actually greatly simplify the algorithm, while avoiding **return** would require writing a lot of unnecessary source code."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "72defe36-0293-4828-8931-ac8c90c39f54",
   "metadata": {},
   "source": [
    "## Debugging functions with **pause** <a name=\"functions_pause\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e16c94fc-4752-47bc-9d9a-1411c4f79388",
   "metadata": {},
   "source": [
    "In this section, we present simple debugging methods that fix most simple bugs in a convenient and efficient way. More specifically, we present the **pause**, **resume** and **abort** statements, \r\n",
    "which are\r\n",
    "- **pause** waits for interactive user input \r\n",
    "- **resume** resumes execution and copy some local variables \r\n",
    "- **abort** interrupts evaluation \r\n",
    "\r\n",
    "A Scilab session usually consists in the definition of new algorithms by the creation of new functions. \r\n",
    "It often happens that a syntax error or an error in the algorithm produces a wrong result.\r\n",
    "\r\n",
    "Consider the problem, the sum of integers from **istart** to **iend**. Again, this simple example is chosen for demonstration purposes, since the **sum** function performs it directly.\r\n",
    "\r\n",
    "The following function **mysum** contains a bug: the second argument \"**foo**\" passed to the **sum** function has no meaning in this context."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "d97eb8b3-4792-42b9-893f-ee5448f896e3",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "Warning : redefining function: mysum                   . Use funcprot(0) to avoid this message\n",
      "\n"
     ]
    }
   ],
   "source": [
    "function y = mysum ( istart , iend )\r\n",
    "  y = sum ( iend : istart , \"foo\" )\r\n",
    "endfunction"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f2756413-8c9c-46c3-a1a8-5427b2ec9a2d",
   "metadata": {},
   "source": [
    "The following session shows what happens when we use the **mysum** function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "7ff75b9c-fa55-41e2-a755-1fae416b0fe8",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "at line     2 of function mysum \n",
      "\n",
      "sum: Wrong value for input argument #2: Must be in the set {\"*\",\"r\",\"c\",\"m\",\"native\",\"double\"}.\n",
      "\n",
      "\n"
     ]
    }
   ],
   "source": [
    "mysum ( 1 , 10 )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5e0a4c33-e835-4afe-9ac8-6b399c36659f",
   "metadata": {},
   "source": [
    "In order to interactively find the problem, we place a **pause** statement inside the body of the function.\n",
    "```\n",
    "function y = mysum ( istart , iend )\n",
    "  pause\n",
    "  y = sum ( iend : istart , \"foo\" )\n",
    "endfunction\n",
    "```\n",
    "\n",
    "We now call the function **mysum** again with the same input arguments.\n",
    "\n",
    "```\n",
    "-->mysum ( 1 , 10 )\n",
    "Type 'resume' or 'abort' to return to standard level prompt.\n",
    "-1->\n",
    "```\n",
    "\n",
    "We are now interactively located **in the body** of the **mysum** function.\n",
    "The prompt \"**-1->**\" indicates that the current call stack is at level -1.\n",
    "We can check the value of the variables **istart** and **iend** by simply typing their names in the console. \n",
    "\n",
    "```\n",
    "-1->istart\n",
    " istart  =\n",
    "    1.  \n",
    "-1->iend\n",
    " iend  =\n",
    "    10.  \n",
    "```\n",
    "\n",
    "In order to progress in our function, we can copy and paste the statements and see what happens interactively, as in the following session.\n",
    "\n",
    "```\n",
    "-1->y = sum ( iend : istart , \"foo\" )\n",
    "y = sum ( iend : istart , \"foo\" )\n",
    "                                  !--error 44 \n",
    "Wrong argument 2.\n",
    "```\n",
    "\n",
    "We can see that the call to the **sum** function does not behave how we might expect. The \"**foo**\" input argument is definitely a bug: we remove it. \n",
    "\n",
    "```\n",
    "-1->y = sum ( iend : istart )\n",
    " y  =\n",
    "    0.  \n",
    "```\n",
    "\n",
    "After the first revision, the call to the **sum** function is now syntactically correct. But the result is still wrong, since the expected result in this case is 55. We see that the **istart** and **iend** variables have been **swapped**. We correct the function call and check that the fixed version behaves as expected\n",
    "\n",
    "```\n",
    "-1->y = sum ( istart : iend )\n",
    " y  =\n",
    "    55.  \n",
    "```\n",
    "\n",
    "The result is now correct. In order to get back to the zero level, we now use the **abort** statement, which interrupts the sequence and immediately returns to the global level.\n",
    "\n",
    "```\n",
    "-1->abort\n",
    "-->\n",
    "```\n",
    "\n",
    "The \"**|-->|**\" prompt confirms that we are now back at the zero level in the call stack.\n",
    "\n",
    "We fix the function definition, which becomes: \n",
    "\n",
    "```\n",
    "function y = mysum ( istart , iend )\n",
    "  pause\n",
    "  y = sum ( istart : iend )\n",
    "endfunction\n",
    "```\n",
    "\n",
    "In order to check our bugfix, we call the function again.\n",
    "\n",
    "```\n",
    "-->mysum ( 1 , 10 )\n",
    "Type 'resume' or 'abort' to return to standard level prompt.\n",
    "-1->\n",
    "```\n",
    "\n",
    "We are now confident about our code, so that we use the **resume** statement, which lets Scilab execute the code as usual.\n",
    "\n",
    "```\n",
    "-->mysum ( 1 , 10 )\n",
    "-1->resume\n",
    " ans  =\n",
    "    55.  \n",
    "```\n",
    "\n",
    "The result is correct. All we have to do is to remove the **pause** statement from the function definition.\n",
    "\n",
    "```\n",
    "function y = mysum ( istart , iend )\n",
    "  y = sum ( istart : iend )\n",
    "endfunction\n",
    "```\n",
    "\n",
    "In this section, we have seen that, used in combination, the **pause**, **resume** and **abort** statements are a very effective way to interactively debug a function. In fact, our example is very simple and the method we presented may appear to be too simple to be convenient. This is not the case. \n",
    "In practice, the **pause** statement has proved to be a very fast way to find and fix bugs, even in very complex situations."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Scilab",
   "language": "scilab",
   "name": "scilab"
  },
  "language_info": {
   "file_extension": ".sci",
   "help_links": [
    {
     "text": "MetaKernel Magics",
     "url": "https://metakernel.readthedocs.io/en/latest/source/README.html"
    }
   ],
   "mimetype": "text/x-scilab",
   "name": "scilab",
   "version": "0.10.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
