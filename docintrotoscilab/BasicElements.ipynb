{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "38386188-5181-407b-8704-925c2931d2d6",
   "metadata": {},
   "source": [
    "# Basic elements of the language"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f3ce15a0-b870-4b38-a59e-fb4bb445d971",
   "metadata": {},
   "source": [
    "## Table of contents\n",
    "1. [Creating real variables](#basic_elements_realvar)\n",
    "2. [Variable names](#basic_elements_namevar)\n",
    "3. [Comments and continuation](#basic_elements_comments)\n",
    "4. [Elementary mathematical functions](#basic_elements_math)\n",
    "5. [Pre-defined mathematical variables](#basic_elements_predefinedmath)\n",
    "6. [Booleans](#basic_elements_booleans)\n",
    "7. [Complex numbers](#basic_elements_complexnumbers)\n",
    "8. [Integers](#basic_elements_integers)\n",
    "    1. [Overview of integers](#basic_elements_overviewint)\n",
    "    2. [Conversion between integers](#basic_elements_conversion)\n",
    "    3. [Circular integers and portability issues](#basic_elements_circular)\n",
    "9. [Strings](#basic_elements_strings)\n",
    "10. [Dynamic type of variables](#basic_elements_dynvar)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b2a7e182-a176-46a5-81b1-1771153bf4da",
   "metadata": {},
   "source": [
    "Scilab is an interpreted language, which means that we can manipulate variables in a very dynamic way. In this section, we present the basic features of the language, that is, we show how to create a real variable, and what elementary mathematical functions can be applied to a real variable. If Scilab provided only these features, it would only be a super desktop calculator. Fortunately, it is a lot more and this is the subject of the remaining sections, where we will show how to manage other types of variables, that is booleans, complex numbers, integers and strings.\n",
    "\n",
    "It seems strange at first, but it is worth to state it right from the start: **in Scilab, everything is a matrix**. To be more accurate, we should write: **all real, complex, boolean, integer, string and polynomial variables are matrices**. Lists and other complex data structures (such as tlists and mlists) are not matrices (but can contain matrices). These complex data structures will not be presented in this document.\n",
    "\n",
    "This is why we could begin by presenting matrices. Still, we choose to present basic data types first, because Scilab matrices are in fact a special organization of these basic building blocks.\n",
    "\n",
    "In Scilab, we can manage real and complex numbers. This always leads to some confusion if the context is not clear enough. In the following, when we write **real variable**, we will refer to a variable which content is not complex. In most cases, real variables and complex variables behave in a very similar way, although some extra care must be taken when complex data is to be processed. Because it would make the presentation cumbersome, we simplify most of the discussions by considering only real variables, taking extra care with complex variables only when needed."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "66282fe4-18a5-4a17-a34f-70ead5c08343",
   "metadata": {},
   "source": [
    "## Creating real variables <a name=\"basic_elements_realvar\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "79935c8f-c1ec-4965-ad02-a7a97928d6fe",
   "metadata": {},
   "source": [
    "\n",
    "Scilab is an interpreted language, which implies that there is no need to declare a variable before using it. Variables are created at the moment where they are first set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "93007bb4-d152-4e59-9f4b-103dd5aeff1d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " x  = \r\n",
      "   1.\r\n",
      " x  = \r\n",
      "   2.\r\n"
     ]
    }
   ],
   "source": [
    "x = 1\n",
    "x = x * 2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "99cacb43-1069-439e-a5a0-8b263fa5d5e9",
   "metadata": {},
   "source": [
    "The value of the variable is displayed each time a statement is executed. That behavior can be suppressed if the line ends with the semicolon <span style=\"color:red\">**;**</span> character."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "3ed220e8-15b5-4390-b0ec-3dfa5cc3d76c",
   "metadata": {},
   "outputs": [],
   "source": [
    "y = 1;\n",
    "y = y * 2;"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0fbcaea5-a696-41f5-b557-c63e931759f4",
   "metadata": {},
   "source": [
    "All the common algebraic operators presented in table are available in Scilab. Notice that the power operator is represented by the hat **$\\hat{\\;}$** character so that computing $x^2$ in Scilab is performed by the **x $\\hat{\\;}$ 2** expression.\n",
    "\n",
    "Scilab elementary mathematical operators: \n",
    "- <span style=\"color:red\">**+**</span>, addition \n",
    "- <span style=\"color:red\">**-**</span>, subtraction \n",
    "- <span style=\"color:red\">**$*$**</span>, multiplication \n",
    "- <span style=\"color:red\">**/**</span>, right division, i.e. $x/y=xy^{-1}$ \n",
    "- <span style=\"color:red\">**$\\backslash$**</span>, left division, i.e. $x\\backslash y=x^{-1}y$ \n",
    "- <span style=\"color:red\">**$\\hat{\\;}$**</span>, power, i.e. $x^y$ \n",
    "- <span style=\"color:red\">**$'$**</span>, transpose conjugate"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bc8d8d3d-14e3-48cf-97f9-e172c5358907",
   "metadata": {},
   "source": [
    "## Variable names <a name=\"basic_elements_namevar\" />"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b89cd62d-d69c-43e5-8ca6-b05927a863ae",
   "metadata": {},
   "source": [
    "Variable names may be as long as the user wants. All ASCII letters from **a** to **z**, from **A** to **Z** and digits from **0** to **9** are allowed, with the additional characters **%**, **_**,  **!**, **$**, **?**. Notice though that variable names, whose first letter is **%**, often have\n",
    "a special meaning in Scilab (pre-defined mathematical variables...).\n",
    "\n",
    "Scilab is case sensitive, which means that upper and lower case letters are considered to be different by Scilab."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "2f321a68-c0e3-4017-8cb1-4f7ae657ba9d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " A  = \r\n",
      "   2.\r\n",
      " a  = \r\n",
      "   1.\r\n"
     ]
    }
   ],
   "source": [
    "A = 2;\n",
    "a = 1;\n",
    "A\n",
    "a"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5905143a-e13a-4e8c-8fc5-6268aaab8b36",
   "metadata": {},
   "source": [
    "## Comments and continuation lines <a name=\"basic_elements_comments\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "99541a63-1b4c-402b-b79c-d4e87f02de3e",
   "metadata": {},
   "source": [
    "Any line which begins with two slashes <span style=\"color:red\">**//**</span> is considered by Scilab as a comment and is ignored. To comment out a block of lines, use the **/* ... */** comments as the C language.\n",
    "\n",
    "When an executable statement is too long to be written on a single line, the second and subsequent lines are called continuation lines. In Scilab, any line which ends with two dots is considered to be the start of a new continuation line.\n",
    "\n",
    "```\n",
    "// This is my comment.\n",
    "--> x = 1 ..\n",
    "  > + 2 ..\n",
    "  > + 3 ..\n",
    "  > + 4\n",
    " x      10.  \n",
    "\n",
    "   10."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3a7b8781-0508-4e8e-8221-acd05fb5ec72",
   "metadata": {},
   "source": [
    "## Elementary mathematical functions <a name=\"basic_elements_math\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fbbedd59-dd62-4521-a4f6-3be9defc8739",
   "metadata": {},
   "source": [
    "This section present a list of elementary mathematical functions. \n",
    "Most of these functions take one input argument and return one output argument. These functions are vectorized in the sense that their input and output arguments are matrices. As a consequence, we can compute data with higher performance, without any loop.\n",
    "\n",
    "In the following example, we use the <span style=\"color:red\">**cos**</span> and <span style=\"color:red\">**sin**</span> functions and check the equality $\\cos(x)^2+\\sin(x)^2=1$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "a2d6d63f-5989-486d-b0bf-d7ba3179c3ce",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " x  = \r\n",
      "  -0.4161468\r\n",
      " y  = \r\n",
      "   0.9092974\r\n",
      " ans  =\r\n",
      "   1.\r\n"
     ]
    }
   ],
   "source": [
    "x = cos(2)\n",
    "y = sin(2)\n",
    "x^2 + y^2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "65a95cfb-fddf-46d8-b1c6-32751830e2cd",
   "metadata": {},
   "source": [
    "For more details on Scilab elementary mathematical functions:\n",
    "\n",
    "- **[trigonometry](https://help.scilab.org/section_99038107015b1d789de50bf92f154a85.html)** functions\n",
    "- **[log - exp - power](https://help.scilab.org/section_04cf1ffa9b1efa819e5ac20bbde28fce.html)** functions\n",
    "- **[sum - prod - min - max](https://help.scilab.org/section_c1be09e30f84df9e317bcfd2ec4e6990.html)** functions"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9c26e095-d95e-46d6-a827-c9282e8bd863",
   "metadata": {},
   "source": [
    "## Pre-defined mathematical variables <a name=\"basic_elements_predefinedmath\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a9f955ae-121b-45c8-b1eb-e4cf23150834",
   "metadata": {},
   "source": [
    "In Scilab, several mathematical variables are pre-defined variables, whose names begin with a percent <span style=\"color:red\">**%**</span> character.\n",
    "The variables which have a mathematical meaning are summarized below:\n",
    "\n",
    "- <span style=\"color:red\">**%i**</span>, the imaginary number\n",
    "- <span style=\"color:red\">**%e**</span>, Euler's number\n",
    "- <span style=\"color:red\">**%pi**</span>, the mathematical constant $\\pi$\n",
    "\n",
    "In the following example, we use the variable <span style=\"color:red\">**%pi**</span> to check the mathematical equality $\\cos(x)^2+\\sin(x)^2=1$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "8a6af156-fc0f-45b5-91d3-673e0c3bc388",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " c  = \r\n",
      "  -1.\r\n",
      " s  = \r\n",
      "   1.225D-16\r\n",
      " ans  =\r\n",
      "   1.\r\n"
     ]
    }
   ],
   "source": [
    "c = cos(%pi)\n",
    "s = sin(%pi)\n",
    "c^2 + s^2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2e55edd3-0abe-4ef8-97d0-a9f988c52a05",
   "metadata": {},
   "source": [
    "The fact that the computed value of $\\sin(\\pi)$ is **not exactly** equal to 0 is a consequence of the fact that Scilab stores the real numbers with floating point numbers, that is, with limited precision. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e8c02ef0-2446-44f0-9842-0d2d34f1cd31",
   "metadata": {},
   "source": [
    "## Booleans <a name=\"basic_elements_booleans\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "090ee7a3-fba8-4439-8590-9f8c9ee14815",
   "metadata": {},
   "source": [
    "Boolean variables can store true or false values. In Scilab, true is written with <span style=\"color:red\">**%t**</span> or <span style=\"color:red\">**%T**</span> and false is written with <span style=\"color:red\">**%f**</span> or <span style=\"color:red\">**%F**</span>.\n",
    "This list presents the several comparison operators which are available in Scilab. These operators return boolean values and take as input arguments all basic data types (i.e. real and complex numbers, integers and strings).  \n",
    "\n",
    "Comparison operators:\n",
    "\n",
    "- **a&b**, logical **and**\n",
    "- **a|b**, logical **or**\n",
    "- **~a**, logical **not**\n",
    "- **a == b** true if the two expressions are equal\n",
    "- **a ~= b** or **a <>b** true if the two expressions are different\n",
    "- **a < b** true if **a** is lower than **b**\n",
    "- **a > b** true if **a** is greater than **b**\n",
    "- **a <= b** true if **a** is lower or equal to **b**\n",
    "- **a >= b** true if **a** is greater or equal to **b**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "0745af34-740b-47c7-ad57-b5a2f8e1add7",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " a  = \r\n",
      "  T\r\n",
      " b  = \r\n",
      "  F\r\n",
      " ans  =\r\n",
      "  F\r\n"
     ]
    }
   ],
   "source": [
    "a = %t\n",
    "b = (0 == 1)\n",
    "a & b"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "03a485e0-10c2-4011-a81e-5652d07f87bd",
   "metadata": {},
   "source": [
    "## Complex numbers <a name=\"basic_elements_complexnumbers\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "22c0b394-4fe0-4e4a-9a7c-c37cf2571763",
   "metadata": {},
   "source": [
    "Scilab provides complex numbers, which are stored as pairs of floating point numbers. \n",
    "The pre-defined variable <span style=\"color:red\">**%i**</span> represents the mathematical imaginary number $i$ which satisfies $i^2=-1$.\n",
    "All elementary functions previously presented before, such as <span style=\"color:red\">**sin**</span> for example, are overloaded for complex numbers. This means that if their input argument is a complex number, the output is a complex number. \n",
    "Here is Scilab complex numbers elementary functions:\n",
    "\n",
    "- [<span style=\"color:red\">**real**</span>](https://help.scilab.org/real.html): real part\n",
    "- [<span style=\"color:red\">**imag**</span>](https://help.scilab.org/imag.html): imaginary part\n",
    "- [<span style=\"color:red\">**imult**</span>](https://help.scilab.org/imult.html): multiplication by $i$, the imaginary unitary\n",
    "- [<span style=\"color:red\">**isreal**</span>](https://help.scilab.org/isreal.html): returns true if the variable has no complex entry\n",
    "\n",
    "\n",
    "In the following example, we set the variable $x$ to $1+i$, and perform several basic operations on it, such as retrieving its real and imaginary parts. Notice how the single quote operator, denoted by <span style=\"color:red\">**'**</span>, is used to compute the conjugate of a complex number."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "b83a8d9c-20c8-4ee3-a0cd-744d737eefb6",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " x  = \r\n",
      "   1. + i  \r\n",
      " ans  =\r\n",
      "  F\r\n",
      " ans  =\r\n",
      "   1. - i  \r\n",
      " y  = \r\n",
      "   1. - i  \r\n",
      " ans  =\r\n",
      "   1.\r\n",
      " ans  =\r\n",
      "  -1.\r\n"
     ]
    }
   ],
   "source": [
    "x = 1 + %i\n",
    "isreal(x)\n",
    "x'\n",
    "y = 1 - %i\n",
    "real(y)\n",
    "imag(y)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c35be6ac-a4c5-4e8f-b6ec-aa3085359907",
   "metadata": {},
   "source": [
    "We finally check that the equality $(1+i)(1-i)=1-i^2=2$ \n",
    "is verified by Scilab."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "931de6c0-7a49-443e-b789-c54c930ce34f",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " ans  =\r\n",
      "   2. + 0.i\r\n"
     ]
    }
   ],
   "source": [
    "x * y"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "60dc0a49-f78b-4581-ab35-7a3014170c0e",
   "metadata": {},
   "source": [
    "## Integers <a name=\"basic_elements_integers\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ebd7ffbe-4011-4270-94e3-77c361632bbd",
   "metadata": {},
   "source": [
    "We can create various types of integer variables with Scilab.  \n",
    "The functions that create such integers are: [<span style=\"color:red\">**int8**</span>](https://help.scilab.org/int8.html), <span style=\"color:red\">**int16**</span>, <span style=\"color:red\">**int32**</span>, <span style=\"color:red\">**int64**</span>, <span style=\"color:red\">**uint8**</span>, <span style=\"color:red\">**uint16**</span>, <span style=\"color:red\">**int32**</span>, <span style=\"color:red\">**int64**</span>. \n",
    "In this section, we first review the basic features of integers, which are associated with a particular range of values. Then we analyze the conversion between integers.\n",
    "In the final section, we consider the behaviour of integers at the boundaries and focus on portability issues."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9cedab27-6160-437c-92c1-0b7343d4a3a5",
   "metadata": {},
   "source": [
    "### Overview of integers <a name=\"basic_elements_overviewint\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28d6c663-4231-4c79-8c8b-c16cb8463d11",
   "metadata": {},
   "source": [
    "There is a direct link between the number of bits used to store an integer and the range of values that the integer can manage. The range of an integer variable depends on the number of its bits:\n",
    "\n",
    "- An $n$-bit signed integer takes its values from the range $[-2^{n-1},2^{n-1}-1]$.\n",
    "- An $n$-bit unsigned integer takes its values from the range $[0,2^n-1]$.\n",
    "\n",
    "For example, an 8-bit signed integer, as created by the <span style=\"color:red\">**int8**</span> function, can store values in the range $[-2^7,2^7-1]$, which simplifies to $[-128,127]$.\n",
    "Here is the map from the type of integer to the corresponding range of values:\n",
    "\n",
    "- **y = int8(x)**, a 8-bit signed integer in $[-2^7,2^7-1]=[-128,127]$\n",
    "- **y = uint8(x)**, a 8-bit unsigned integer in $[0,2^8-1]=[0,255]$\n",
    "- **y = in16(x)**, a 16-bit signed integer in $[-2^{15},2^{15}-1]=[-32768,32767]$\n",
    "- **y = uint16(x)**, a 16-bit unsigned integer in $[0,2^{16}-1]=[0, 65535]$\n",
    "- **y = int32(x)**, a 32-bit signed integer in $[-2^{31},2^{31}-1]$\n",
    "- **y = uint32(x)**, a 32-bit unsigned integer in $[0,2^{32}-1]=[0, 4294967295]$\n",
    "- **y = int64(x)**, a 64-bit signed integer in $[-2^{63},2^{63}-1]$\n",
    "- **y = uint64(x)**, a 64-bit unsigned integer in $[0,2^{64}-1]=[0, 18446744073709551615]$\n",
    "\n",
    "In the following session, we check that an unsigned 32-bit integer has values inside the range $[0, 2^{32}-1]$, which simplifies to $[0, 4294967295]$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "b2c46952-2b3b-4956-ae7b-2e1520e2ccc2",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " n  = \r\n",
      "   32.\r\n",
      " ans  =\r\n",
      "   4294967295.\r\n",
      " i  = \r\n",
      "  0\r\n",
      " j  = \r\n",
      "  4294967295\r\n",
      " k  = \r\n",
      "  0\r\n"
     ]
    }
   ],
   "source": [
    "format(25)\n",
    "n = 32\n",
    "2^n-1\n",
    "i = uint32(0)\n",
    "j = i - 1\n",
    "k = j + 1\n",
    "format(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1c567988-5922-4dd1-990c-bf39ea5be78f",
   "metadata": {},
   "source": [
    "### Conversion between integers <a name=\"basic_elements_conversion\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "48da979b-f76a-44cb-bfc5-9cb996fc122c",
   "metadata": {},
   "source": [
    "There are functions which convert to and from integer data types:\n",
    "- [<span style=\"color:red\">**iconvert**</span>](https://help.scilab.org/iconvert.html), conversion to integer representation\n",
    "- [<span style=\"color:red\">**inttype**</span>](https://help.scilab.org/inttype.html), type of integers\n",
    "\n",
    "The <span style=\"color:red\">**inttype**</span> function inquires about the type of an integer variable. Depending on the type, the function returns a corresponding value, as\n",
    "- 1 & 8-bit signed integer,\n",
    "- 2 & 16-bit signed integer,\n",
    "- 4 & 32-bit signed integer,\n",
    "- 11 & 8-bit unsigned integer,\n",
    "- 12 & 16-bit unsigned integer,\n",
    "- 14 & 32-bit unsigned integer.\n",
    "\n",
    "When two integers are added, the types of the operands are analyzed: the resulting integer type is the larger, so that the result can be stored. In the following script, we create an 8-bit integer **i** (which is associated with **inttype**=1) and a 16-bit integer **j** (which is associated with **intype==2**). \n",
    "The result is stored in **k**, a 16-bit signed integer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "6be324a4-7cff-4a1f-9311-450e378c56c5",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " i  = \r\n",
      "  1\r\n",
      " ans  =\r\n",
      "   1.\r\n",
      " j  = \r\n",
      "  2\r\n",
      " ans  =\r\n",
      "   2.\r\n",
      " k  = \r\n",
      "  3\r\n",
      " ans  =\r\n",
      "   2.\r\n"
     ]
    }
   ],
   "source": [
    "i = int8(1)\n",
    "inttype(i)\n",
    "j = int16(2)\n",
    "inttype(j)\n",
    "k = i + j\n",
    "inttype(k)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ec248ce9-ec52-471d-b67a-a2a7c98a74b2",
   "metadata": {},
   "source": [
    "### Circular integers and portability issues <a name=\"basic_elements_circular\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9280c382-2377-4c77-8480-4356109fe8de",
   "metadata": {},
   "source": [
    "The behaviour of integers at the range boundaries deserves a particular analysis, since it is different from software to software. \n",
    "In Scilab, the behaviour is **circular**, that is, if an integer at the upper limit is incremented, the next value is at the lower limit.  \n",
    "An example of circular behaviour is given in the following session, where"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "4127f870-9870-43a9-9fd9-edf44e4f492e",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " ans  =\r\n",
      "  252  253  254  255  0  1  2  3  4\r\n",
      " ans  =\r\n",
      "  252  253  254  255  0  1  2  3  4\r\n",
      " ans  =\r\n",
      "  124  125  126  127 -128 -127 -126 -125 -124\r\n"
     ]
    }
   ],
   "source": [
    "uint8(0 + (-4:4))\n",
    "uint8(2^8 + (-4:4))\n",
    "int8(2^7 + (-4:4))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6cb59f6e-964f-408e-b6e1-b02d69c3fe05",
   "metadata": {},
   "source": [
    "This is in contrast with other mathematical packages, such as Octave or Matlab.\n",
    "In these packages, if an integer is at the upper limit, the next integer stays at the upper limit. In the following Octave session, we execute the same computations \n",
    "as previously.\n",
    "\n",
    "```\n",
    "octave-3.2.4.exe:1> uint8(0+(-4:4))\n",
    "ans =\n",
    "  0  0  0  0  0  1  2  3  4\n",
    "octave-3.2.4.exe:5> uint8(2^8+(-4:4))\n",
    "ans =\n",
    "  252  253  254  255  255  255  255  255  255\n",
    "octave-3.2.4.exe:2> int8(2^7+(-4:4))\n",
    "ans =\n",
    "  124  125  126  127  127  127  127  127  127\n",
    "```\n",
    "\n",
    "The Scilab circular way gives a greater flexibility in the processing of integers, since we can write algorithms with fewer **if** statements.\n",
    "But these algorithms must be checked, particularly if they involve the boundaries of the integer range. Moreover, translating a script from another computation system \n",
    "into Scilab may lead to different results."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eba4d75d-8ce0-45c4-a0e5-08c258029e75",
   "metadata": {},
   "source": [
    "## Strings <a name=\"basic_elements_strings\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5ee49c9c-1868-4e2d-943b-0bdb31ad9ef7",
   "metadata": {},
   "source": [
    "Strings can be stored in variables, provided that they are delimited by simple <span style=\"color:red\">**'**</span> or double quotes <span style=\"color:red\">**\"**</span>. \n",
    "The concatenation operation is available from the <span style=\"color:red\">**+**</span> operator. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "02205c6a-31a5-40c4-bb71-bfa787a80076",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " x  = \r\n",
      "  \"foo\"\r\n",
      " y  = \r\n",
      "  \"bar\"\r\n",
      " ans  =\r\n",
      "  \"foobar\"\r\n"
     ]
    }
   ],
   "source": [
    "x = \"foo\"\n",
    "y = \"bar\"\n",
    "x + y"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2112b3f5-7fc1-4195-9925-1cec391bc4b0",
   "metadata": {},
   "source": [
    "For more details on strings, see help page: **[strings](https://help.scilab.org/section_a08ff639ee9ce4bd16aa1c6ac0f86c69.html)**."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "736bbe75-54ff-499d-8fb7-c4b9b8177059",
   "metadata": {},
   "source": [
    "## Dynamic type of variables <a name=\"basic_elements_dynvar\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f721f911-c25b-40be-b81e-fb5634c5538b",
   "metadata": {},
   "source": [
    "When we create and manage variables, Scilab changes the variable type dynamically. This means that we can create a real value, and then put a string variable in it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "ac0ae069-8478-47b2-968c-5f2047dd4f39",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " x  = \r\n",
      "   1.\r\n",
      " ans  =\r\n",
      "   2.\r\n",
      " x  = \r\n",
      "  \"foo\"\r\n",
      " ans  =\r\n",
      "  \"foobar\"\r\n"
     ]
    }
   ],
   "source": [
    "x = 1\n",
    "x + 1\n",
    "x = \"foo\"\n",
    "x + \"bar\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bee46772-9abb-4a9c-96e4-c938f1c46d09",
   "metadata": {},
   "source": [
    "We emphasize here that Scilab is not a typed language, that is, we do not have to declare the type of a variable before setting its content. \n",
    "Moreover, the type of a variable can change during the life of the variable."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Scilab",
   "language": "scilab",
   "name": "scilab"
  },
  "language_info": {
   "file_extension": ".sci",
   "help_links": [
    {
     "text": "MetaKernel Magics",
     "url": "https://metakernel.readthedocs.io/en/latest/source/README.html"
    }
   ],
   "mimetype": "text/x-scilab",
   "name": "scilab",
   "version": "0.10.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
